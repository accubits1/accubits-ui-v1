import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from "../core/api.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  loginForm
  loginSubmitted = false;
  
  signupForm
  signUpSubmitted = false;
  signupValue : any = {}

  constructor(
    private formBuilder : FormBuilder,
    private apiService : ApiService,
    private toastr : ToastrService,
    private router : Router
  ) {
    this.loginForm = this.formBuilder.group({
      email : ['', [Validators.required,Validators.email]],
      password : ['', [Validators.required,Validators.minLength(5)]]
    })

    this.signupForm = this.formBuilder.group({
      email : ['', [Validators.required,Validators.email]],
      password : ['', [Validators.required,Validators.minLength(5)]],
      confirm_password : ['', [Validators.required,Validators.minLength(5)]],
      name : ['', [Validators.required]]
    })
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }
  get g() { return this.signupForm.controls; }
  
  ngOnInit(): void {
  }

  async onLogin(value){
    this.loginSubmitted = true
    //If values are invalid
    if (this.loginForm.invalid) {
      return;
    }
    let response = await this.apiService._post('/user/login',value).toPromise()
    console.log(response)
    if(response.status==true){
      localStorage.setItem('jwt',response.token)
      this.router.navigate(['home'])
      this.toastr.success(response.message)
    }else{
      this.toastr.warning(response.error.error)
    }
  }

  async signUp(value){
    this.signUpSubmitted = true
    if (this.signupForm.invalid) {
      return;
    }
    if(value.password != value.confirm_password){
      this.toastr.success("Mismatch Passwords")
      return
    }
    let resp = await this.apiService._post('/user',value).toPromise()
    if(resp.status==true){
      this.signUpSubmitted = false
      this.signupForm.reset()
      this.toastr.success(resp.message)
    }else{
      this.toastr.warning(resp.error.error)
    }
  }

}
