import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

import { jwtService } from './jwt.service'

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(
    private jwtService: jwtService,
    private router : Router
  ) { 
  }

  canActivate(): boolean {
    if (this.jwtService.getJwtToken()) {
      return true;
    } else {
      this.router.navigate(['login'])  
      return false;
    }
  }
}
