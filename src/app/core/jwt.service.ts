import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class jwtService {

  constructor(
  ) { }

  getJwtToken(){
      return localStorage.getItem('jwt')
  }

  storeJwtToken(jwt: string) {
    localStorage.setItem('jwt', jwt);
  }
}
